# GCS OIDC Authenticating Proxy

**Purpose**

This is a quick demo app written in go showing proxying files from GCS, protected by an OIDC provider hosted login. It is not intended to be used as a production implementation and is not coded to any set of coding best practices. Its goal is merely to demonstrate the conceptual possibility of wiring SSO into a static website hosted in GCS buckets.

The cloud storage proxy work traces its inspiration from https://github.com/domZippilli/gcs-proxy-cloud-run, but is vastly simplified to not include the pipelines. This project simply reads and serves content from a GCS bucket.

**Okta Integration**
 
The OIDC implementation uses [Okta](www.okta.com) and was copied from https://github.com/okta/samples-golang/tree/master/okta-hosted-login but follows the standard OIDC (Oauth2) Authorization Code flow. This implementation leverages an Okta provided library to verify the Json Web Token (JWT) returned from Okta. However, there is nothing proprietary to Okta in the code as it is a standard signed JWT verification. As this has not been tested with other providers, there may be differences with providers who may require PKCE challenges as part of the flow (See Okta's description here https://developer.okta.com/docs/guides/implement-grant-type/authcodepkce/main/#create-the-proof-key-for-code-exchange).
 

 
At present the solution expects you to use the following environment variables
- *ISSUER* - The OIDC issuer (In okta this is your tenant org such as https://{yourOktaTenantDomain}/oauth2/default)
- *CLIENT_ID* - The OIDC provider issued Client ID for you application
- *CLIENT_SECRET* - The OIDC provider issued Client Secret for you application
- *HOST* - The HOST name where the app will be running. This is used to construct the URI's used in the flow. The value also must be configured in the Okta OIDC provider.
- *GCS_BUCKET* - The Google Cloud Storage bucket containing the static content.
(Note the above *CLIENT_ID* and *CLIENT_SECRET* values should likely be supplied using Google Secret Manager, but can also be supplied using environment variables as is doen in this demo)

### Deploying on Cloud Run ###

**Prequisites:**
- An existing OIDC provider account at Okta (or other provider such as Azure Active Directory)
- A configured application in the OIDC provider
- GCP project with Cloud Run enabled
- Static web page content in a Google Cloud Storage bucket
- Service account for Cloud Run service that has permissions to read from Cloud Storage Bucket
- Permissions to deploy services to cloud run including ability to deploy with service with "Allow unauthenticated invocations" setting.

1. Build the images and deploy to Cloud Registry or Artifact Repository.
2. Update the included crun-env file for environment settings. (Note if using Cloud Run generated URLs you may need to depoy a demo container to determine the generated URL which will be of the form `{svcname}-{cloudRunGenerated}-{regionCode}.a.run.app`)
3. Use the below command to deploy to cloud run.
 
    `gcloud run deploy gcs-proxy-oidc --image {imagePath} --env-vars-file crun-env --allow-unauthenticated --region {region}`
